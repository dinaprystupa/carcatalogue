﻿using CarsCatalogue.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CarsCatalogue.Controllers
{
    public class HomeController : Controller
    {

        CarContext db = new CarContext();
        public ActionResult Index()
        {
            IEnumerable<Car> cars = db.Cars;
            return View(cars);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public RedirectResult Create(Car car)
        {
            db.Cars.Add(car);
            db.SaveChanges();
            return Redirect("/Home/Index");
        }
        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return HttpNotFound();
            Car car = GetCarById(id);
            if (car == null)
                return HttpNotFound();
            return View(car);
        }
        [HttpPost]
        public RedirectResult Delete(Car car)
        {
            db.Entry(car).State = System.Data.Entity.EntityState.Deleted;
            db.SaveChanges();
            return Redirect("/Home/Index");
        }
        public ActionResult Details(int? id)
        {
            if (id == null)
                return HttpNotFound();
            Car car = GetCarById(id);
            if (car == null)
                return HttpNotFound();
            return View(car);
        }
        [HttpGet]
        public ActionResult Edit (int? id)
        {
            if (id == null)
                return HttpNotFound();
            Car car = GetCarById(id);
            if (car == null)
                return HttpNotFound();
            return View(car);
        }
        [HttpPost]
        public ActionResult Edit(Car updatedCar)
        {
            db.Entry(updatedCar).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return Redirect("/Home/Index");
        }

        public ActionResult Query(string filterOn, string filter, string sortBy, string sortOrder)
        {
            ViewBag.FilterOn = filterOn;
            ViewBag.Filter = filter;
            ViewBag.SortBy = sortBy;
            ViewBag.SortOrder = sortOrder;

            return View("Index", Sort(Filter(db.Cars, filterOn, filter), sortBy, sortOrder));
        }

        private IEnumerable<Car> Sort (IEnumerable<Car> cars, string sortBy, string sortOrder)
        {
            switch(sortBy)
            {
                case "year":
                    cars = cars.OrderBy(x => x.Year);
                    break;
                case "manufacturer":
                    cars = cars.OrderBy(x => x.Manufacturer.ToString());
                    break;
                case "model":
                    cars = cars.OrderBy(x => x.Model);
                    break;
                default:
                    break;
            }
            if (sortOrder == "desc")
            {
                cars = cars.Reverse();
            }
            return cars;
        }
        private IEnumerable<Car> Filter(IEnumerable<Car> cars, string filterOn, string filter)
        {
            switch(filterOn)
            {
                case "manufacturer":
                    cars = cars.Where(x => x.Manufacturer.ToString() == filter);
                    break;
                case "model":
                    cars = cars.Where(x => x.Model == filter);
                    break;
                case "year":
                    cars = cars.Where(x => x.Year.ToString() == filter);
                    break;
                default:
                    break;
            }
            return cars;
        }

        private Car GetCarById (int? id)
        {
            return db.Cars.Find(id);
        }
    }
}
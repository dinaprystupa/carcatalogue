﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarsCatalogue.Models
{
    public class Car
    {
        public int Id { get; set; }
        public String LicensePlateNumber { get; set; }
        public String Model { get; set; }
        public Manufacturer Manufacturer { get; set; }
        public int Year { get; set; }
        public int intColor { get { return _color.ToArgb(); } set { _color = System.Drawing.Color.FromArgb(value); } }
        public System.Drawing.Color Color { get { return _color; } set { _color = value; } }
        private System.Drawing.Color _color;

    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CarsCatalogue.Models
{
    public class CarDbInitializer : DropCreateDatabaseAlways<CarContext>
    {
        protected override void Seed(CarContext db)
        {
            db.Cars.Add(new Car { LicensePlateNumber = "AO456", Model = "Focus", Manufacturer = Manufacturer.Ford, intColor = System.Drawing.Color.Aquamarine.ToArgb(), Year = 2014 });
            db.Cars.Add(new Car { LicensePlateNumber = "AA789", Model = "Civic", Manufacturer = Manufacturer.Honda, intColor = System.Drawing.Color.Yellow.ToArgb(), Year = 1998 });
            db.Cars.Add(new Car { LicensePlateNumber = "BA356", Model = "Leaf", Manufacturer = Manufacturer.Nissan, intColor = System.Drawing.Color.Black.ToArgb(), Year = 2017 });
            db.Cars.Add(new Car { LicensePlateNumber = "BB433", Model = "Corrola", Manufacturer = Manufacturer.Toyota, intColor = System.Drawing.Color.Green.ToArgb(), Year = 2010 });
            db.Cars.Add(new Car { LicensePlateNumber = "AO123", Model = "E-TRON", Manufacturer = Manufacturer.Audi, intColor = System.Drawing.Color.Black.ToArgb(), Year = 2002 });
            db.Cars.Add(new Car { LicensePlateNumber = "AB321", Model = "Mercedes", Manufacturer = Manufacturer.Mercedes, intColor = System.Drawing.Color.WhiteSmoke.ToArgb(), Year = 2015 });
            db.Cars.Add(new Car { LicensePlateNumber = "BN823", Model = "Focus", Manufacturer = Manufacturer.Ford, intColor = System.Drawing.Color.White.ToArgb(), Year = 2015 });
            db.Cars.Add(new Car { LicensePlateNumber = "AX132", Model = "X4", Manufacturer = Manufacturer.BMW, intColor = System.Drawing.Color.DarkGray.ToArgb(), Year = 2012 });
            db.Cars.Add(new Car { LicensePlateNumber = "XO231", Model = "Toyota", Manufacturer = Manufacturer.Volkswagen, intColor = System.Drawing.Color.DarkSeaGreen.ToArgb(), Year = 2008 });
            db.Cars.Add(new Car { LicensePlateNumber = "AB213", Model = "A7", Manufacturer = Manufacturer.Audi, intColor = System.Drawing.Color.Silver.ToArgb(), Year = 2010 });
            db.Cars.Add(new Car { LicensePlateNumber = "XB132", Model = "Leaf", Manufacturer = Manufacturer.Nissan, intColor = System.Drawing.Color.CornflowerBlue.ToArgb(), Year = 2013 });
            db.Cars.Add(new Car { LicensePlateNumber = "UI582", Model = "QASHQAI", Manufacturer = Manufacturer.Nissan, intColor = System.Drawing.Color.DarkRed.ToArgb(), Year = 2017 });
            db.Cars.Add(new Car { LicensePlateNumber = "DA307", Model = "Focus", Manufacturer = Manufacturer.Ford, intColor = System.Drawing.Color.DarkGray.ToArgb(), Year = 2016 });
            db.Cars.Add(new Car { LicensePlateNumber = "AK002", Model = "QASHQAI", Manufacturer = Manufacturer.Nissan, intColor = System.Drawing.Color.Orange.ToArgb(), Year = 2015 });
            db.Cars.Add(new Car { LicensePlateNumber = "BX321", Model = "Camry", Manufacturer = Manufacturer.Toyota, intColor = System.Drawing.Color.LawnGreen.ToArgb(), Year = 2017 });
            db.Cars.Add(new Car { LicensePlateNumber = "XA312", Model = "M5", Manufacturer = Manufacturer.BMW, intColor = System.Drawing.Color.Cyan.ToArgb(), Year = 2017 });

            base.Seed(db);
        }
    }
}
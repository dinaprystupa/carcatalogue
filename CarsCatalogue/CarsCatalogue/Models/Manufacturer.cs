﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarsCatalogue.Models
{
    public enum Manufacturer
    {
        Audi,
        Ford,
        Volkswagen,
        Honda,
        Mercedes,
        Nissan,
        BMW,
        Toyota
    }
}